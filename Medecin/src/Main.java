import donnee.serialiseur.Serialiseur;
import donnee.stub.CabinetStub;
import interfaces.IStrategie;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import view.PrincipalControleur;
import viewmodel.CabinetVM;

import java.io.IOException;

public class Main extends Application {
    private IStrategie strategie;
    private PrincipalControleur controleur;

    @Override
    public void start(Stage stage) throws Exception {
        CabinetVM cabinetVM;
        try {
            strategie = new Serialiseur();
            cabinetVM = new CabinetVM(strategie.charger());
        } catch (IOException | ClassNotFoundException e) {
            strategie = new CabinetStub();
            cabinetVM = new CabinetVM(strategie.charger());
        }
        System.out.println(strategie.getClass().getName());
        controleur = new PrincipalControleur(cabinetVM);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FenetrePrincipal.fxml"));
        loader.setController(controleur);
        stage.setScene(new Scene(loader.load()));
        stage.show();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        new Serialiseur().sauvegarder(controleur.getViewmodel().getModel());
    }
}

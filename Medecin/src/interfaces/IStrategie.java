package interfaces;

import model.Cabinet;

import java.io.IOException;

/**
 * @author Thomas VINCENT.
 */
public interface IStrategie {
    /**
     * Permet de définir la méthode chargement.
     * @param cabinet Cabinet du Model.
     * @throws IOException Cette exception est levée si la méthode ne trouve pas le fichier en question.
     */
    void sauvegarder(Cabinet cabinet) throws IOException;

    /**
     *
     * @return Retourne un Cabinet du Model.
     * @throws IOException Cette exception est levée si la méthode ne trouve pas le fichier en question.
     * @throws ClassNotFoundException Cette exception est levée si la méthode n'arrive pas à lire le Cabinet.
     */
    Cabinet charger() throws IOException, ClassNotFoundException;
}

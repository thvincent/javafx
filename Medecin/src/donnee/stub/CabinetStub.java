package donnee.stub;

import interfaces.IStrategie;
import model.Cabinet;
import model.patient.Patient;

public class CabinetStub implements IStrategie {
    @Override
    public void sauvegarder(Cabinet cabinet) {
        return;
    }

    @Override
    public Cabinet charger() {
        Cabinet cabinet = new Cabinet();

        Patient patient1 = new Patient();
        patient1.setNom("VINCENT");
        patient1.setPrenom("Thomas");
        patient1.setAge(20);

        Patient patient2 = new Patient();
        patient2.setNom("ROUDIER");
        patient2.setPrenom("Chloé");
        patient2.setAge(20);

        cabinet.ajouterPatient(patient1);
        cabinet.ajouterPatient(patient2);

        return cabinet;
    }
}

package donnee.serialiseur;

import interfaces.IStrategie;
import model.Cabinet;

import java.io.*;

public class Serialiseur implements IStrategie {
    @Override
    public void sauvegarder(Cabinet cabinet) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("save.bin"));
        oos.writeObject(cabinet);
    }

    @Override
    public Cabinet charger() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("save.bin"));
        return (Cabinet)ois.readObject();
    }
}

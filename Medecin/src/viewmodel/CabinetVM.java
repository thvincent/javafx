package viewmodel;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Cabinet;
import model.patient.Patient;
import viewmodel.patient.PatientVM;

import java.beans.IndexedPropertyChangeEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class CabinetVM implements PropertyChangeListener {
    private Cabinet model;

    public Cabinet getModel() {
        return model;
    }

    private ObservableList<PatientVM> patientsVMObservableList = FXCollections.observableArrayList();
    private ListProperty<PatientVM> patientsVM = new SimpleListProperty<>(patientsVMObservableList);
        public ListProperty<PatientVM> patientsVMProperty() { return patientsVM; }

    public ObservableList<PatientVM> getPatientsVMObservableList() {
        return patientsVMObservableList;
    }

    public CabinetVM(Cabinet cabinet) {
        model = cabinet;
        model.ajouterListener(this);
        model.getPatients().forEach(patient -> {
            patientsVM.add(new PatientVM(patient));
            System.out.println("Ajout en VM du patient : " + patient.getNom());
        });
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {
        if(event.getPropertyName().equals(Cabinet.PROP_PATIENTS)){
            patientsVM.add(((IndexedPropertyChangeEvent) event).getIndex(), new PatientVM((Patient) event.getNewValue()));
        }
    }

    public void ajouterPatientListe(){
        Patient p = new Patient();

        p.setNom("-- Inconnu");
        p.setPrenom("Inconnu --");
        p.setAge(0);

        model.ajouterPatient(p);
    }
}

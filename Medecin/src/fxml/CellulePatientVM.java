package fxml;

import javafx.scene.control.ListCell;
import viewmodel.patient.PatientVM;

public class CellulePatientVM extends ListCell<PatientVM> {
    @Override
    protected void updateItem(PatientVM item, boolean empty) {
        super.updateItem(item, empty);
        if(!empty){
            textProperty().bind(item.nomProperty().concat(" ").concat(item.prenomProperty()));
        }
        else{
            textProperty().unbind();
            setText("");
        }
    }
}

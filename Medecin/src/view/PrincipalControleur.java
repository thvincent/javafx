package view;

import fxml.CellulePatientVM;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.patient.Patient;
import viewmodel.CabinetVM;
import viewmodel.patient.PatientVM;

public class PrincipalControleur {
    private CabinetVM viewmodel;

    public CabinetVM getViewmodel() {
        return viewmodel;
    }

    @FXML
    private ListView<PatientVM> listView;
    @FXML
    private TextField nomTextField;
    @FXML
    private TextField prenomTextField;
    @FXML
    private TextField ageTextField;

    public PrincipalControleur(CabinetVM cabinetVM) {
        this.viewmodel = cabinetVM;
    }

    public void initialize(){
        listView.setCellFactory((__) -> new CellulePatientVM());
        listView.itemsProperty().bind(viewmodel.patientsVMProperty());
        listView.getSelectionModel().selectedItemProperty().addListener((__,oldV,newV) -> initilizeItemBinding(oldV,newV));
    }

    private void initilizeItemBinding(PatientVM oldV, PatientVM newV) {
        if(oldV != null){
            nomTextField.textProperty().unbindBidirectional(oldV.nomProperty());
            prenomTextField.textProperty().unbindBidirectional(oldV.prenomProperty());
            ageTextField.textProperty().unbindBidirectional(oldV.ageProperty());
        }
        if(newV != null){
            nomTextField.textProperty().bindBidirectional(newV.nomProperty());
            prenomTextField.textProperty().bindBidirectional(newV.prenomProperty());
            ageTextField.textProperty().bind(newV.ageProperty().asString());
        }
    }

    @FXML
    private void quitter(){
        Stage stage = (Stage) nomTextField.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void ajouterPatientListe(){
        viewmodel.ajouterPatientListe();
    }

    @FXML
    private void ajouterPatientFenetre(){
    }
}

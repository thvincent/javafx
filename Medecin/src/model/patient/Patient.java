package model.patient;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

/**
 * @author Thomas VINCENT.
 */

public class Patient implements Serializable {
    public static final transient String PROP_NOM = "Propriété Nom";
    public static final transient String PROP_PRENOM = "Propriété Prénom";
    public static final transient String PROP_AGE = "Propriété Age";
    private transient PropertyChangeSupport support = new PropertyChangeSupport(this);

    public PropertyChangeSupport getSupport() {
        if(support == null){
            support = new PropertyChangeSupport(this);
        }
        return support;
    }

    private String nom;
    private String prenom;
    private int age;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        String old = this.nom;
        this.nom = nom;
        getSupport().firePropertyChange(PROP_NOM, old, nom);
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        String old = this.prenom;
        this.prenom = prenom;
        getSupport().firePropertyChange(PROP_PRENOM, old, prenom);
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        int old = this.age;
        this.age = age;
        getSupport().firePropertyChange(PROP_AGE, old, age);
    }

    public void ajouterListener(PropertyChangeListener listener){
        getSupport().addPropertyChangeListener(listener);
    }
}

package model;

import model.patient.Patient;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Cabinet implements Serializable {
    public static final transient String PROP_PATIENTS = "Propriété patients";
    private transient PropertyChangeSupport support = new PropertyChangeSupport(this);

    public PropertyChangeSupport getSupport() {
        if(support == null){
            support = new PropertyChangeSupport(this);
        }
        return support;
    }

    private List<Patient> patients = new ArrayList<>();

    public List<Patient> getPatients() {
        return Collections.unmodifiableList(patients);
    }

    public void ajouterPatient(Patient patient){
        patients.add(patient);
        getSupport().fireIndexedPropertyChange(PROP_PATIENTS, patients.indexOf(patient), null, patient);
    }

    public void ajouterListener(PropertyChangeListener listener){
        getSupport().addPropertyChangeListener(listener);
    }
}

package viewmodel.patient;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import model.patient.Patient;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class PatientVM implements PropertyChangeListener {
    private Patient model;

    private StringProperty nom = new SimpleStringProperty();
        public String getNom() { return nom.get(); }
        public StringProperty nomProperty() { return nom; }
        public void setNom(String nom) { this.nom.set(nom); }

    private StringProperty prenom = new SimpleStringProperty();
        public String getPrenom() { return prenom.get(); }
        public StringProperty prenomProperty() { return prenom; }
        public void setPrenom(String prenom) { this.prenom.set(prenom); }

    private IntegerProperty age = new SimpleIntegerProperty();
        public int getAge() { return age.get(); }
        public IntegerProperty ageProperty() { return age; }
        public void setAge(int age) { this.age.set(age); }

    public PatientVM(Patient patient){
        model = patient;
        model.ajouterListener(this);
        initializeProperty();
        initializeListener();
    }

    private void initializeProperty(){
        nom.set(model.getNom());
        prenom.set(model.getPrenom());
        age.set(model.getAge());
    }

    private void initializeListener(){
        nom.addListener((__,___,newV) -> model.setNom(newV));
        prenom.addListener((__,___,newV) -> model.setPrenom(newV));
        age.addListener((__,___,newV) -> model.setAge((int)newV));
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {
        if(event.getPropertyName().equals(Patient.PROP_NOM)){
            nom.set((String) event.getNewValue());
        }
        if(event.getPropertyName().equals(Patient.PROP_PRENOM)){
            prenom.set((String) event.getNewValue());
        }
        if(event.getPropertyName().equals(Patient.PROP_AGE)){
            age.set((int) event.getNewValue());
        }
    }
}
